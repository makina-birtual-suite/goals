# ABOUT PROJECT
This project is the implementation of a register-based [process virtual machine](https://en.wikipedia.org/wiki/Virtual_machine).

The architecture is @vladlu 's design.

The objective of this project is to have a virtual machine capable of running any program at a decent speed and efficiency.

This is also a research project for static memory management and compile time [garbage collection](https://en.wikipedia.org/wiki/Garbage_collection_(computer_science)).

# FAQ

## Why?
Why not? Programming is supposed to be fun and useful. Giving fellow programmers a toy VM and language to incentivise learning is all we strive for as students.

## How fast is it?
It's fairly decent. On the rule110 example, an equivalent code in C is only 4 times faster.

## Why isn't feature "x" implemented?
Maybe we didn't think about it yet! Submit an issue, and contribute if you are willing. 

## How can I join a team?
You can contact the project lead. We are searching for a platform to host the project information on.

## I made a project based on Makina!
Great! If your project is open-source AND not GPL (as this project is FreeBSD-licenced), we can link it up on this Group if you want to.

## Who are you?
University students at Faculty of Engineering of the University of Porto (FEUP), studying computer engineering (LEIC).

## What hardware are you developing this on?
Asus Vivobook S14, R7 5700U processor, 16GB of ram.

## Where is the documentation?
While @ezrea1y is not finished with writting up proper documentation, you can check out the [makina-lib](https://gitlab.com/makina-birtual-suite/makina-lib) src files. lib.rs has a slightly outdated documentation (asm inst names are slightly wrong) on the top of the page. 

## What platforms are supported?
Any in which a rust compiler runs, for now.

## Dependencies?
Hopefully just the rust toolchain, for now, in the Core Tooling.
Extra tooling might have more dependencies, like a C/C++ compiler or a Python interpreter. As Extra Tools are essentially divorced from the Core Tooling, they should indicate their dependencies.



# PROJECT GOALS

- [x] Implement Single Threaded, simple, Virtual Machine.

- [x] Implement Bytecode Compiler for STsVM.

- [ ] Implement Debugger for STsVM.

- [ ] Implement Disassembler for STsVM 

- [ ] Update Docummentation.

- [ ] Create proper wiki with documentation.

- [ ] Develop more examples.

- [ ] Create Sample language that targets STsVM

- [ ] Implement Multi Threaded, simple, Virtual Machine.

- [ ] (...)



# PROJECT NON-GOALS
- Achieve max performance. Being very fast is desirable, but we are not there yet.
- (...)


# PROJECT'S TEAM
## Project's Lead
- Guilherme Duarte Silva Matos @vladlu
## Core Tools Team
### Team Lead
- Guilherme Duarte Silva Matos @vladlu
### Core Tools' Designer Team
- Guilherme Duarte Silva Matos @vladlu
### Core Tool's Maintainer Team
- Guilherme Duarte Silva Matos @vladlu

## Extra Tools Team
### Team Lead 
- Guilherme Duarte Silva Matos @vladlu
### Extra Tools' Designer Team
- Guilherme Duarte Silva Matos @vladlu
- João Vítor da Costa Ferreira @ezrea1y
### Extra Tools' Maintainer Team
- João Vítor da Costa Ferreira @ezrea1y

## Documentation Team
### Team Lead
- João Vítor da Costa Ferreira @ezrea1y
### Maintainers
- João Vítor da Costa Ferreira @ezrea1y
- Guilherme Duarte Silva Matos @vladlu













